from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.model_selection import KFold
from functools import partial
import multiprocessing as mp
import numpy as np
import logging

from visualization import plotting
from utils import my_io

SEED = 4316932


class Trainer(object):
    """
    """

# ______________________________________________________________________________________________________________________
    def read_features_from_file(self, inputs):
        """ read the feature matrices
        :param inputs: list of hdf5 files
        :return: list of feature matrices
        """
        datas = len(inputs) * [None]
        for in_id, in_file in enumerate(inputs):
            datas[in_id] = my_io.read_hdf5(in_file)
        return datas

# ______________________________________________________________________________________________________________________
    def take_subset_of_feats(self, data, subsets, split_id):
        """ the labels are arranged as follows: <domain>_<feature>_<freq band>_<electrode>
        :param data: ndarray n_samples x n_features of a class
        :param subsets: can be either domains, or features or electrodes
        :param split_id: the location where domain, feature or electrode can be found in the feature name
        :return: reduced list of features as specified by subsets. selected features are restored by self.feature_labels
        """
        if 'all' in subsets:
            return data
        else:
            # if doing electrodes make sure the given query matches the internal name convention (upper case)
            if split_id == -1:
                subsets = [subset.upper() for subset in subsets]
            new_feature_label_ids = []

            for subset in subsets:
                for feature_label_id, feature_label in enumerate(self.feature_labels):
                    if split_id is not None:
                        if subset in feature_label.split('_')[split_id]:
                            new_feature_label_ids.append(feature_label_id)
                    else:
                        if subset in feature_label:
                            new_feature_label_ids.append(feature_label_id)

            # only pick those columns from the features that correspond to the subset of features / electrodes
            if not new_feature_label_ids:
                return data

            new_feature_label_ids = np.asarray(sorted(np.unique(new_feature_label_ids)))
            data = data[:, new_feature_label_ids]

            # also adapt the features_labels
            self.feature_labels = self.feature_labels[new_feature_label_ids]
            return data

# ______________________________________________________________________________________________________________________
    def spawn_classifier(self, process_id, x_train_fold, y_train_fold, x_test_fold, y_test_fold, out_dir):
        """ creates a subprocess to do one fold of cv
        :param process_id: the id of the process
        :param x_train_fold: training data
        :param y_train_fold: training labels
        :param x_test_fold: testing data
        :param y_test_fold: testing labels
        :param out_dir: output directory
        :return: process id, evaluation metrics
        """
        # include in subprocesses, since top level include overrides logging preferences and suppresses all of this
        # codes logging
        import autosklearn.classification
        # this was taken from auto-sklearn website and adapted
        tmp_folder = '/'.join([out_dir, 'autoskl', 'tmp', str(process_id)])
        output_folder = '/'.join([out_dir, 'autoskl', 'output', str(process_id)])

        clf = autosklearn.classification.AutoSklearnClassifier(
            time_left_for_this_task=self.total_budget,
            per_run_time_limit=self.run_budget,
            ml_memory_limit=4096,
            tmp_folder=tmp_folder,
            output_folder=output_folder,
            delete_tmp_folder_after_terminate=False,
            delete_output_folder_after_terminate=False,
            seed=SEED,
        )
        clf.fit(x_train_fold, y_train_fold)
        predictions = clf.predict(x_test_fold)

        acc = accuracy_score(y_test_fold, predictions)
        prec = precision_score(y_test_fold, predictions)
        rec = recall_score(y_test_fold, predictions)
        f1 = f1_score(y_test_fold, predictions)

        return process_id, [acc, prec, rec, f1]

# ______________________________________________________________________________________________________________________
    def worker(self, in_q, out_q, func):
        while not in_q.empty():
            i, train, labels_train, test, labels_test = in_q.get(timeout=1)
            out_q.put((func(i, train, labels_train, test, labels_test)))

# ______________________________________________________________________________________________________________________
    def train_test_split(self, n_samples):
        """ create ids to pick train and test fold from feature matrices
        :param n_samples: number of samples of the class
        :return: lists holding indices for every train and test fold
        """
        ids = np.arange(n_samples)
        train_fold_ids, test_fold_ids = [], []
        kf = KFold(n_splits=self.n_folds)
        for train, test in kf.split(ids):
            train_fold_ids.append(train)
            test_fold_ids.append(test)

        return np.asarray(train_fold_ids), np.asarray(test_fold_ids)

# ______________________________________________________________________________________________________________________
    def prune_feature_space_wrt_cmd_args(self, features_list, domain, feats, elecs):
        # picks the subsets of the feature matrices as specified by the domain cmd argument
        features_list = self.take_subset_of_feats(features_list, domain, 0)
        # picks the subsets of the feature matrices as specified by the feats cmd argument
        features_list = self.take_subset_of_feats(features_list, feats, None)
        # picks the subsets of the feature matrices as specified by the elecs cmd argument
        features_list = self.take_subset_of_feats(features_list, elecs, -1)
        return features_list

# ______________________________________________________________________________________________________________________
    def write_results(self, cmd_args, n_samples_class1, n_samples_class2):
        # upload the results to google spreadsheet
        if not cmd_args.no_up:
            my_io.write_to_google_result_tracking(
                cmd_args.input,
                [n_samples_class1, n_samples_class2],
                cmd_args.window,
                cmd_args.windowsize,
                cmd_args.overlap,
                self.clf_name,
                '?',  # estimators
                cmd_args.domain,  # domain
                cmd_args.feats,  # feats
                cmd_args.elecs,  # elecs
                len(self.feature_labels),  # len of feature_labels
                cmd_args.bands,
                0,  # band overlap
                '?',  # scaler
                self.n_folds,
                [self.total_budget, self.run_budget],
                self.get_results(),
                # TODO: add classifier information here
                ''  # '' '.join(re.split('\s{2,}', str(clf))),
                ''  # str(clf.show_models()) # notes
            )

        # write the results of cv
        my_io.write_results(cmd_args.output, cmd_args.input, self.get_results())


# ______________________________________________________________________________________________________________________
    def train(self, cmd_args):
        """ take the features computed in the preprocessing step. split them into train and test folds to perform CV.
        :param cmd_args: output directory and visualization is read from cmd_args
        """
        #
        if self.feature_labels is None:
            self.feature_labels = my_io.read_feature_labels(cmd_args.output)

        # read data from file
        # prune the feature space as specified by cmd arguments feats, elecs and domain. makes use of feature labels
        # generate fold indices for train and test to pick from data
        datas = self.read_features_from_file(cmd_args.input)
        train_fold_ids, test_fold_ids = len(datas) * [None], len(datas) * [None]
        for class_id, data in enumerate(datas):
            data = self.prune_feature_space_wrt_cmd_args(data, cmd_args.domain, cmd_args.feats, cmd_args.elecs)
            data_train_fold_ids, data_test_fold_ids = self.train_test_split(len(data))
            train_fold_ids[class_id] = data_train_fold_ids
            test_fold_ids[class_id] = data_test_fold_ids

        [normal_data, abnormal_data] = datas
        [normal_train_fold_ids, abnormal_train_fold_ids] = train_fold_ids
        [normal_test_fold_ids, abnormal_test_fold_ids] = test_fold_ids

        # set up multiprocessing
        manager = mp.Manager()
        in_q, out_q = manager.Queue(maxsize=self.n_folds), manager.Queue(maxsize=self.n_folds)
        processes = np.ndarray(shape=(self.n_folds,), dtype=mp.Process)
        partial_process = partial(self.spawn_classifier, out_dir=cmd_args.output)

        # parellalize cv folds. but only start that many processes as specified by n_proc
        # for each cv fold pick data, generate labels and hand it to a subprocess
        worked_folds = 0
        while worked_folds < self.n_folds:
            for process_id in range(worked_folds, min(worked_folds+self.n_jobs, self.n_folds)):
                normal_train = normal_data[normal_train_fold_ids[process_id]]
                abnormal_train = abnormal_data[abnormal_train_fold_ids[process_id]]
                train = np.vstack((normal_train, abnormal_train))
                labels_train = len(normal_train) * [0] + len(abnormal_train) * [1]

                normal_test = normal_data[normal_test_fold_ids[process_id]]
                abnormal_test = abnormal_data[abnormal_test_fold_ids[process_id]]
                test = np.vstack((normal_test, abnormal_test))
                labels_test = len(normal_test) * [0] + len(abnormal_test) * [1]

                in_q.put((process_id-worked_folds, train, labels_train, test, labels_test))
                processes[process_id] = mp.Process(target=self.worker, args=(in_q, out_q, partial_process))

            for process_id, process in enumerate(processes[worked_folds:worked_folds+self.n_jobs]):
                logging.info("\t\tTraining and predicting fold {}.".format(worked_folds+process_id))
                process.start()

            for process in processes[worked_folds:worked_folds+self.n_jobs]:
                process.join()

            # catch the results as computed by the subprocesses
            self.results[worked_folds:worked_folds+self.n_jobs] = self.catch_results(out_q)
            worked_folds += self.n_jobs

        # write the results
        self.write_results(cmd_args, len(normal_data), len(abnormal_data))
        # visualize the results
        if 'all' in cmd_args.visualize or 'train' in cmd_args.visualize:
            plotting.plot_cv_results(cmd_args.output, self.n_folds, self.get_results())


# ______________________________________________________________________________________________________________________
    def get_results(self):
        """ compute mean and std of cv fold metrics accuracy, precision, recall and f1-score """
        mean_results = np.mean(self.results, axis=0) * 100
        std_results = np.std(self.results, axis=0) * 100

        # 0: accs, 1: precs, 2: recs, 3: f1s
        return [self.results[:, 0] * 100, mean_results[0], std_results[0]], \
               [self.results[:, 1] * 100, mean_results[1], std_results[1]], \
               [self.results[:, 2] * 100, mean_results[2], std_results[2]], \
               [self.results[:, 3] * 100, mean_results[3], std_results[3]]

# ______________________________________________________________________________________________________________________
    def catch_results(self, out_q):
        """ from each subprocess take the results from out_q """
        results = out_q.qsize() * [None]
        while not out_q.empty():
            fold_id, fold_results = out_q.get()
            results[fold_id] = fold_results
        return results

# ______________________________________________________________________________________________________________________
    def __init__(self, auto_skl, n_folds=10, n_jobs=1):
        self.n_folds = n_folds
        self.n_jobs = n_jobs
        self.clf_name = 'ASC'

        [self.total_budget, self.run_budget] = auto_skl
        self.feature_labels = None

        self.results = np.ndarray(shape=(self.n_folds, 4), dtype=float)


