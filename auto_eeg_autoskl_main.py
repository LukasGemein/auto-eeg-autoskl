#!/usr/bin/env python3.5
from datetime import datetime, date
import textwrap as _textwrap
import argparse
import warnings
import logging
import os

from utils import my_io
import pipeline


# ______________________________________________________________________________________________________________________
class LineWrapRawTextHelpFormatter(argparse.RawDescriptionHelpFormatter):
    # this makes the help look much nicer since it allows for a higher line width
    def _split_lines(self, text, width):
        text = self._whitespace_matcher.sub(' ', text).strip()
        return _textwrap.wrap(text, 96)


# ______________________________________________________________________________________________________________________
def main(cmd_args, not_known):
    today, now = date.today(), datetime.time(datetime.now())

    warnings.filterwarnings("ignore", category=DeprecationWarning)

    # create output directories
    my_io.check_out(cmd_args.output, cmd_args.input)
    logname = os.path.join(cmd_args.output, ''.join(cmd_args.output.split('/')[-2:]) + '.log')
    formatter = logging.Formatter("[%(levelname)s] (%(module)s:%(lineno)d) %(message)s")
    logging.basicConfig(level=cmd_args.verbosity)

    rootlogger = logging.getLogger()
    rootlogger.handlers = []

    # add a logger that logs to console
    streamhandler = logging.StreamHandler()
    streamhandler.setFormatter(formatter)
    rootlogger.addHandler(streamhandler)

    # add a logger that logs to file
    filhandler = logging.FileHandler(logname)
    filhandler.setFormatter(formatter)
    rootlogger.addHandler(filhandler)

    logging.info('\tStarted on {} at {}.'.format(today, now))

    if not_known:
        logging.error('\tFollowing parameters could not be interpreted: {}.'.format(not_known))

    if cmd_args.window != 'boxcar' or cmd_args.windowsize != 2 or cmd_args.overlap != 0 or cmd_args.bands != '0,4,8,13,18,24,30,60,120':
        logging.warning("\tWindow, windowsize, overlap and bands cannot be specified. They are preprocessing settings.")

    pl = pipeline.Pipeline()
    pl.run(cmd_args)
    
    logging.info('\tFinished on {} at {}.\n\n'.format(date.today(), datetime.time(datetime.now())))

# ______________________________________________________________________________________________________________________
if __name__ == '__main__':

    parser = argparse.ArgumentParser(formatter_class=LineWrapRawTextHelpFormatter)

    # non-optional
    parser.add_argument('input', type=str, nargs='+', help='list of feature files')
    parser.add_argument('output', type=str, help='output directory')
    parser.add_argument('auto_skl', type=int, nargs=2, metavar='', default=[600, 120],
                        help='specifies the total and run time budget of autosklearn.')

    # these cannot be set in this run. a warning is displayed --------------------------
    parser.add_argument('-w', '--window', type=str, default='boxcar', metavar='',
                        help='cannot be set. this is a preprocessing setting')

    parser.add_argument('-ws', '--windowsize', '--window-size', type=float, default=2, metavar='',
                        help='cannot be set. this is a preprocessing setting')

    parser.add_argument('--overlap', type=int, default=0, metavar='',
                        help='cannot be set. this is a preprocessing setting')

    parser.add_argument('--bands', type=str, nargs='+', metavar='', default='0,4,8,13,18,24,30,60,120',
                        help='cannot be set. this is a preprocessing setting')

    # in theory this could be set

    parser.add_argument('--subset', type=int, default=None, metavar='', 
                        help='if a performance estimate on a random subset of the data is desired')

    # ---------------------------

    # optional
    parser.add_argument('--visualize', type=str, nargs='+', metavar='', default=['train', 'pred', 'post'],
                        help='specify what steps of the pipeline should be visualized. choose between: ' +
                             ', '.join(['all', 'pre', 'train', 'pred', 'post', 'none']))

    parser.add_argument('--verbosity', type=int, default=20, metavar='', choices=[0, 10, 20, 30, 40, 50],
                        help='verbosity level of logging: ' + ', '.join(str(s) for s in [0, 10, 20, 30, 40, 50]))

    parser.add_argument('--no-up', '--noup', action='store_true',
                        help='use this option if you do NOT want to upload the results to google spreadsheet.')

    parser.add_argument('--n-proc', '--nproc', type=int, default=2, metavar='',
                        help='number of processes used to read data and compute features')

    parser.add_argument('--domain', type=str, nargs='+', metavar='', default=['all'],
                        choices=['patient', 'fft', 'time', 'pyeeg', 'dwt', 'sync', 'all'],
                        help='list of features to be used for classification.'
                             'choose between: ' + ', '.join(['patient', 'fft', 'time', 'pyeeg', 'dwt', 'sync', 'all']))

    parser.add_argument('--feats', type=str, nargs='+', metavar='', default=['all'],
                        help='list of features to be used for classification. take a look at file "features.list" for'
                              'choices.')

    parser.add_argument('--elecs', type=str, nargs='+', metavar='', default=['all'],
                        help='list of electrodes to be used for classification. choose between the electrodes of the'
                             'international 10-20 placement.')

    cmd_arguments, unknown = parser.parse_known_args()
    main(cmd_arguments, unknown)
