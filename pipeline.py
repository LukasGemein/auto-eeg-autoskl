import logging

from training import autoskl_trainer
from utils import my_io


class Pipeline(object):
    """ The pipeline object is a container for all the steps for the machine learning pipeline, i.e. preprocessor,
    trainer, predictor, preprocessor. Moreover, there is a stats object that gathers information about a pipeline run.
    """

# ______________________________________________________________________________________________________________________
    def run(self, cmd_args):
        # update cmd args such as window and window size as stored in the feature file name
        cmd_args = my_io.update_cmd_args(cmd_args, cmd_args.input)
        keys = cmd_args.__dict__
        logging.info('\t\t\tUpdated the cmd arguments to ...\n\t\t\t\t\t' +
                     '\n\t\t\t\t\t'.join(map(lambda x: ': '.join([x, str(keys[x])]), keys)))


# -------------------------------------------- TRAINING / CV / AUTOSKLEARN --------------------------------------------#
        self.trainer = autoskl_trainer.Trainer(n_jobs=cmd_args.n_proc, auto_skl=cmd_args.auto_skl)
        self.trainer.train(cmd_args)
        # TODO: return the classifier, parameters that lead to result that can be used for prediction on test set
        results = self.trainer.get_results()
        metrics = ['acc:', 'prec:', 'rec:', 'f1:']
        for metric_id in range(len(metrics)):
            logging.info(metrics[metric_id])
            logging.info(results[metric_id])

# ---------------------------------------------------- PREDICTION -----------------------------------------------------#
#             self.predictor = predictor.Predictor()
#             self.predictor.predict(feature_files)


# -------------------------------------------- POSTPROCESSING / ANALYSIS ----------------------------------------------#
        # self.postprocessor = postprocessor.Postprocessor(feature_files, window_counts, rec_names)
        # self.postprocessor.postprocess(cmd_args)

# ______________________________________________________________________________________________________________________
    def __init__(self):
        self.trainer = None
        self.predictor = None
        self.postprocessor = None

